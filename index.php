<?php require_once "./code.php"?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>A03: Classes and Objects</title>
  </head>
  <body>

    <h1>Person</h1>
	  <p><?php echo $person->printName(); ?></p>

    <h1>Developer</h1>
      <p><?= $developer->printName(); ?></p>

    <h1>Engineer</h1>
      <p><?= $engineer->printName(); ?></p>
    
  </body>
</html>
